# `vt_tracker` - Segmenting vocal tract articulators in RT-MRI

`vt_tracker` is a Python library developed by the <a href="https://team.inria.fr/multispeech/">Multispeech</a>
team at <a href="https://www.loria.fr/">Loria</a> for tracking vocal tract articulators in real-time
magnetic resonance imaging (RT-MRI).

The articulators that are covered by our method are:

<ul>
    <li>Arytenoid cartilage</li>
    <li>Epiglottis</li>
    <li>Lower lip</li>
    <li>Pharyngeal wall</li>
    <li>Soft palate midline</li>
    <li>Thyroid cartilage</li>
    <li>Tongue</li>
    <li>Upper lip</li>
    <li>Vocal folds</li>
</ul>

## How to use?

1. Clone vt_tools repo

```
>> git clone git@gitlab.inria.fr:vsouzari/vt_tools.git
```

2. Clone the repo

```
>> git clone git@gitlab.inria.fr:vsouzari/vt_tracker.git
```

3. Install the libs

```
>>> pip3 install -e /path/to/vt_tools
>>> pip3 install -e /path/to/vt_tracker
```

4. Import and use the code

```
import vt_tracker
```

## How to contribute?

1. Clone vt_tools repo

```
>> git clone git@gitlab.inria.fr:vsouzari/vt_tools.git
```

2. Clone the repo

```
>> git clone git@gitlab.inria.fr:vsouzari/vt_tracker.git
```

3. Create and activate your virtual environment

```
>> cd vt_tracker
>> python3 -m venv .dev_env
>> source .dev_env/bin/activate
```

4. Install the requirements

```
>> pip3 install -e ../vt_tools
>> pip3 install -r requirements.txt
```

5. Run the tests

```
>> pytest --doctest-modules
```

## How to cite

If you use `vt_tracker` in scientific publications, please, cite it using the following Bibtex entry:

```
@article{ribeiro4192628automatic,
  title={Automatic Tracking of Vocal Tract Articulators in Real-Time Magnetic Resonance Imaging},
  author={Ribeiro, Vin{\'\i}cius and Isaieva, Karyna and Leclere, Justine and Karpinski, Romain and Felblinger, Jacques and Vuissoz, Pierre-Andr{\'e} and Laprie, Yves},
  journal={Available at SSRN 4192628}
}
```
