import pytest
import torch

from vt_tracker.border_segmentation import detect_borders


def test_unrecognized_device():
    bs, channels, width, height = 1, 3, 64, 64
    batch = torch.zeros(size=(bs, channels, width, height))
    device = "qwerty"

    with pytest.raises(Exception):
        detect_borders(batch, device)


def test_unavailable_cuda():
    bs, channels, width, height = 1, 3, 64, 64
    batch = torch.zeros(size=(bs, channels, width, height))
    device = "cuda"

    with pytest.raises(Exception):
        detect_borders(batch, device)


def test_invalid_shape_batch():
    channels, width, height = 3, 64, 64
    batch = torch.zeros(size=(channels, width, height))
    device = "cpu"

    with pytest.raises(Exception):
        detect_borders(batch, device)


def test_detect_borders_empty_input():
    bs, channels, width, height = 4, 3, 64, 64
    batch = torch.zeros(size=(bs, channels, width, height))
    device = "cpu"

    outputs = detect_borders(batch, device)

    assert len(outputs) == bs
