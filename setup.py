from setuptools import setup, find_packages

requirements = [
    "funcy==1.16",
    "numba>=0.54.1",
    "numpy>=1.21.2",
    "opencv-python==4.7.0.72",
    "Pillow==8.4.0",
    "pydicom==2.2.2",
    "pytest==6.2.5",
    "scikit-image==0.18.3",
    "torch>=1.11",
    "torchvision>=0.10.0",
]

setup(
    name="vt_tracker",
    version="0.0.1",
    description="Tracking vocal tract articulators with deep learning",
    author="Vinicius Ribeiro",
    author_email="vinicius.souza-ribeiro@loria.fr",
    license="MIT",
    packages=find_packages(),
    zip_safe=False,
    install_requires=requirements
)
