import argparse
import matplotlib.pyplot as plt
import os
import torch

from glob import glob
from torchvision import transforms
from vt_tools import COLORS
from vt_tools.bs_regularization import regularize_Bsplines
from vt_tracker.border_segmentation import detect_borders
from vt_tracker.input import InputLoaderMixin, imagenet_normalize
from vt_tracker.postprocessing.calculate_contours import calculate_contour

"""
Vocal tract tracker tutorial:

VT_tracker works in two phases. In the first, a CNN runs on the input image to segment the
articulators' borders. In the second, each segmentation mask goes through a post-processing
algorithm to retrieve the output curve.

1) The CNN is trained on RGB images in which to retrieve the contours of time step t, the R channel
receives the time step t-1, G channel corresponds to time step t, and B channel is time step t+1.
To run this script, run
`python3 tutorial.py --red /path/to/t-1.dcm --green /path/to/t.dcm --blue /path/to/t+1.dcm`
The script also works if the image is saved as `npy` files. In this case, just replace `.dcm` by
`.npy` in the command line above.

2) The model input is a torch tensor of size is 136 x 136 pixels. For this reason we need
transforms.Resize and transforms.ToTensor.

3) InputLoaderMixin is a class that implements all the input loading steps. Just call
InputLoaderMixin.load_input with the proper arguments. The function is prepared to load grayscale
inputs. If that is what you want, pass None as r_filepath and b_filepath, and model="gray", but keep
in mind that it might decrease model's performance.

4) The model is trained with images normalized w.r.t. ImageNet. For this reason, import
imagenet_normalize and pass the torch.tensor to it.

5) The function detect_borders will run the CNN model and return a list with the lenght of batch
size. Each item in the list corresponds to one image and is a dictionary containing the segmentation
mask, a bounding box enveloping the articulator, and a classification score.

6) The function calculate_contour corresponds to the second phase. It receives as input the name of
the articulator (detect_borders output key) and a segmentation mask (detect_borders output value).
The function's output will be a tensor of shape (50, 2).

7) An additional postprocessing is b-splines regularization, to have a smoother contour. The
function regularize_Bsplines receive the contour as input and return the regularized x- and
y-coordinates, which can be drawn on top of the image for visualization.
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--red", dest="r_filepath", default=None)
    parser.add_argument("--green", dest="g_filepath")
    parser.add_argument("--blue", dest="b_filepath", default=None)
    args = parser.parse_arguments()

    resize = transforms.Resize(size=(136, 136))
    to_tensor = transforms.ToTensor()

    r_filepath = args.r_filepath if os.path.exists(args.r_filepath) else None
    g_filepath = args.g_filepath
    b_filepath = args.b_filepath if os.path.exists(args.b_filepath) else None

    if not os.path.exists(g_filepath):
        raise FileNotFoundError(g_filepath)

    input_ = InputLoaderMixin.load_input(
        r_filepath,
        g_filepath,
        b_filepath,
        mode="rgb",
        resize=resize
    )
    input_ = imagenet_normalize(to_tensor(input_))
    batch = input_.unsqueeze(dim=0)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    detected = detect_borders(
        batch,
        device=device
    )

    contours = {}
    for articulator, detection in detected[0].items():
        contour = calculate_contour(articulator, detection["mask"])
        contours[articulator] = contour

    lw = 5
    plt.figure(figsize=(10, 10))
    plt.imshow(input_.permute(1, 2, 0))
    for articulator, contour in contours.items():
        reg_x, reg_y = regularize_Bsplines(contour, degree=2)
        contour_reg = torch.tensor([reg_x, reg_y])
        plt.plot(*contour_reg, lw=lw, color=COLORS[articulator])
    plt.axis("off")
    plt.tight_layout()
    plt.savefig("figure.jpg")
