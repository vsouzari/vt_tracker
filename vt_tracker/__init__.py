import os

from vt_tools import (
    ARYTENOID_CARTILAGE,
    EPIGLOTTIS,
    LOWER_INCISOR,
    LOWER_LIP,
    PHARYNX,
    SOFT_PALATE,
    SOFT_PALATE_MIDLINE,
    THYROID_CARTILAGE,
    TONGUE,
    UPPER_INCISOR,
    UPPER_LIP,
    VOCAL_FOLDS
)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BORDER_SEGMENTATION_FILEPATH = os.path.join(BASE_DIR, "resources", "vocal_tract_tracking.pt")
DENTAL_ARTICULATION_FILEPATH = os.path.join(BASE_DIR, "resources", "dental_articulation.pt")

GRAPH_BASED = "graph-based"
SKIMAGE = "skimage"
