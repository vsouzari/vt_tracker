import cv2
import funcy
import numpy as np

from scipy.ndimage import binary_fill_holes
from skimage.measure import find_contours
from skimage.morphology import skeletonize

from . import POST_PROCESSING
from .. import (
    ARYTENOID_CARTILAGE,
    EPIGLOTTIS,
    GRAPH_BASED,
    PHARYNX,
    SKIMAGE,
    SOFT_PALATE,
    SOFT_PALATE_MIDLINE,
    TONGUE,
    VOCAL_FOLDS
)
from .graph_based import (
    connect_points_graph_based,
    detect_extremities_on_axis,
    detect_tails,
    find_contour_points
)


def normalize_interval(v, new_min, new_max):
    """
    Linearly rescales an array such that the values are in the interval [new_min, new_max].

    Args:
        v (np.ndarray): Array to normalize.
        new_min (float): Minimal value of the desired interval.
        new_max (float): Maximum value of the desired interval.

    >>> v = np.array([0.2, 0.35, 0.41, 0.5])
    >>> normalize_interval(v, 0., 1.)
    array([0. , 0.5, 0.7, 1. ])
    """
    v_min = v.min()
    v_max = v.max()

    x = (v - v_min) / (v_max - v_min)
    y = x * (new_max - new_min)

    return y


def rescale_contour(contour, s_in, s_out):
    """
    Proportionaly rescales a contour to a given size. Similar behavior of rescaling an image.

    Args:
        contour (np.ndarray): Array to rescale.
        s_in (float): Input size.
        s_out (float): Output size.

    >>> c = np.array([0.2, 0.3, 0.4, 0.5])
    >>> rescale_contour(c, 1., 2.)
    array([0.4, 0.6, 0.8, 1. ])
    """
    r = s_out / s_in
    return np.array(contour) * r


def upscale_mask(mask_, upscale):
    """
    Rescales an image providing a rescale_contour instance to rescale the derived contour back to
    the original size.

    Args:
        mask_ (np.ndarray): Segmentation mask to be upscaled.
        upscale (np.ndarray): Target size of uscaling.
    """
    mask = mask_.copy()

    if upscale is None:
        return mask, lambda x: x

    s_out, _ = mask.shape
    s_in = upscale
    mask = cv2.resize(mask, (upscale, upscale), interpolation=cv2.INTER_CUBIC)
    rescale_contour_fn = funcy.partial(rescale_contour, s_in=s_in, s_out=s_out)

    return mask, rescale_contour_fn


def threshold_array(arr_, threshold, high=1., low=0.):
    """
    Returns a new array with the same shape of the input array such that the positions in the input
    array with value lower or equal to threshold are 'low' and and the positions with value greater
    than threshold are 'high'.

    Args:
        arr_ (np.ndarray): Input array to threshold.
        threshold (float): Threshold value to compare with.
        high (float): Value that is attributed to positions greater than the threshold.
        low (float): Value that is attributed to positions lower or equal to the threshold.

    >>> arr = np.array([0.2, 0.3, 0.4, 0.5, 0.6, 0.7])
    >>> threshold_array(arr, 0.4, 1., 0.)
    array([0., 0., 0., 1., 1., 1.])
    """
    arr = arr_.copy()
    arr[arr <= threshold] = low
    arr[arr > threshold] = high

    return arr


def contour_bbox_area(contour):
    """
    Calculates the area of the smallest bounding box that contains all the points of the contour.

    Args:
        contour (np.ndarray): Array of shape (N, 2).

    >>> contour = np.array([(0., 0.), (0., 1.), (1., 0.)])
    >>> contour_bbox_area(contour)
    1.0
    """

    x0 = contour[:, 0].min()
    y0 = contour[:, 1].min()
    x1 = contour[:, 0].max()
    y1 = contour[:, 1].max()

    return (x1 - x0) * (y1 - y0)


def calculate_contours_with_skimage(mask, threshold, **kwargs):
    """
    Finds the largest (in area) iso-valued contour in a 2D-array.
    """
    mask_thr = threshold_array(mask, threshold)
    contours = sorted(find_contours(mask_thr), key=contour_bbox_area, reverse=True)

    if len(contours) == 0:
        return []

    contour = np.flip(contours[0])
    return contour


def calculate_contours_with_graph(
    mask, threshold, r, alpha, beta, gamma, delta, articulator, G, gravity_curve, **kwargs
):
    """
    Builds a graph with the pixels that have a value greater than the threshold, finds the
    extremities of the contour, and connects the contour using the graph based algorithm.
    For information about the weights, readh the documentation of
    vt_tracker.postprocessing.graph_based.calculate_edge_weights.

    Args:
        mask (np.ndarray): Border segmentation mask.
        threshold (float): Threshold value to include pixels in the graph.
        r (float): Maximum distance to connect pixels.
        alpha (float): Weight of the node distance component.
        beta (float): Weight of the intensity component.
        gamma (float): Weight of the previous_contour component.
        delta (float): Weight of the angular distance component.
        articulator (str): Name of the articulator.
        G (float): Weight of the (5) component.
        gravity_curve (np.ndarray): Reference curve for the gravity weighting.
    """
    if articulator != TONGUE:
        mask_thr = threshold_array(mask, threshold)

        if articulator == SOFT_PALATE:
            mask_thr = binary_fill_holes(mask_thr)

        mask_thr = skeletonize(mask_thr).astype(np.uint8)
    else:
        mask_thr = mask.copy()
        mask_thr[mask_thr <= threshold] = 0.

        mask_thr[np.where(mask_thr > 0.)] = normalize_interval(
            mask_thr[np.where(mask_thr > 0.)], 0., 1.
        )

    contour_points, cm = find_contour_points(mask_thr)
    if articulator in (SOFT_PALATE, SOFT_PALATE_MIDLINE, VOCAL_FOLDS):
        source, sink = detect_extremities_on_axis(contour_points, axis=0)
    elif articulator in (PHARYNX, EPIGLOTTIS):
        source, sink = detect_extremities_on_axis(contour_points, axis=1)
    elif articulator == ARYTENOID_CARTILAGE:
        # In the case of the arytenoid cartilage, we want to change the reference to calculate the
        # maximal angular distance. We need to keep the same y-position, however, we move the
        # x-coordinate to the end of the image, so the maximum appeature is given by the complete
        # extension of the muscle.
        _, w = mask_thr.shape
        cm = (w - 1, cm[1])
        source, sink, _ = detect_tails(contour_points, cm)
    else:
        source, sink, _ = detect_tails(contour_points, cm)

    if source is None or sink is None:
        return []

    contour, _, _ = connect_points_graph_based(
        mask_thr,
        contour_points,
        r, alpha, beta, gamma, delta,
        tails=(source, sink),
        G=G,
        gravity_curve=gravity_curve
    )

    return contour


def _calculate_contour_threshold_loop(
    post_processing_fn, mask, threshold, articulator, alpha, beta, gamma, delta,
    G=0.0, gravity_curve=None, max_iter=10, min_length=10
):
    """
    Runs a threshold reduction loop to guarantee a contour with a minimal lenght of 10 points.
    For information about the weights, read the documentation of
    vt_tracker.postprocessing.graph_based.calculate_edge_weights.

    Args:
        post_processing_fn (function): The post processing function to calculate the contour.
        mask (np.ndarray): Border segmentation mask to calculate the contour.
        threshold (float): Initial loop threshold.
        articulator (str): Name of the articulator.
        alpha (float): Weight of the node distance component.
        beta (float): Weight of the intensity component.
        gamma (float): Weight of the previous_contour component.
        delta (float): Weight of the angular distance component.
        articulator (str): Name of the articulator.
        G (float): Weight of the (5) component.
        gravity_curve (np.ndarray): Reference curve for the gravity weighting.
        max_iter (int): Maximum number of iterations
        min_length (int): Contour minimal length
    """
    min_threshold = 0.0005
    contour = []

    i = 0
    while len(contour) < min_length and i < max_iter:
        contour = post_processing_fn(
            mask=mask,
            threshold=threshold,
            articulator=articulator,
            r=3,
            alpha=alpha,
            beta=beta,
            gamma=gamma,
            delta=delta,
            G=G,
            gravity_curve=gravity_curve
        )

        threshold = threshold * 0.8
        if threshold < min_threshold:
            break
        i += 1

    if len(contour) < min_length:
        contour = []

    return contour


def calculate_contour(articulator, mask, gravity_curve=None, cfg=None):
    """
    Calculates the contour for a given border segmentation map.

    Args:
        articulator (str): Name of the articulator.
        mask (np.ndarray): Border segmentation mask to calculate the contour.
        gravity_curve (np.ndarray): Reference curve for the gravity weighting.
        cfg (namedtuple): vt_tracker.postprocessing.PostProcessingCfg instance.
    """
    if cfg is None and articulator not in POST_PROCESSING:
        raise Exception(
            f"Class '{articulator}' does not have post-processing parameters configured"
        )

    if cfg is None:
        cfg = POST_PROCESSING[articulator]

    method_name = cfg["method"]
    if method_name not in METHODS:
        raise Exception(f"Unavailable post-processing method '{method_name}'")

    min_length = cfg["min_length"]
    max_upscale_iter = cfg["max_upscale_iter"]
    for i in range(1, max_upscale_iter + 1):
        upscale = i * cfg["upscale"]

        # If we upscale before post-processing, we need to define a function to rescale the
        # generated contour. Else, we use an identity function as a placeholder.
        new_mask, rescale_contour_fn = upscale_mask(mask, upscale)

        rescaled_gravity_curve = None
        if gravity_curve is not None:
            s_in, _ = mask.shape
            rescaled_gravity_curve = rescale_contour(gravity_curve, s_in=s_in, s_out=upscale)

        post_processing_fn = METHODS[method_name]
        contour = _calculate_contour_threshold_loop(
            post_processing_fn,
            new_mask,
            cfg["threshold"],
            articulator,
            cfg["alpha"],
            cfg["beta"],
            cfg["gamma"],
            cfg["delta"],
            cfg["G"],
            rescaled_gravity_curve,
            min_length=min_length
        )

        if len(contour) > min_length:
            break

    if np.isnan(contour).any():
        raise Exception("contour has nan")
    contour = rescale_contour_fn(contour)

    return contour


METHODS = {
    GRAPH_BASED: calculate_contours_with_graph,
    SKIMAGE: calculate_contours_with_skimage
}
