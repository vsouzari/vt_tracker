import os
import yaml

post_processing_dir = os.path.dirname(os.path.abspath(__file__))
cfg_filpath = os.path.join(post_processing_dir, "cfg.yaml")

with open(cfg_filpath) as f:
    POST_PROCESSING = yaml.safe_load(f)
