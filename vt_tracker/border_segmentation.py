import funcy
import torch

from functools import lru_cache
from torchvision.models.detection.mask_rcnn import maskrcnn_resnet50_fpn, MaskRCNN_ResNet50_FPN_Weights

from . import (
    ARYTENOID_CARTILAGE,
    EPIGLOTTIS,
    LOWER_LIP,
    PHARYNX,
    SOFT_PALATE_MIDLINE,
    BORDER_SEGMENTATION_FILEPATH,
    THYROID_CARTILAGE,
    TONGUE,
    UPPER_LIP,
    VOCAL_FOLDS
)

# The current version of the model was trained to detect only five classes, which are the lower lip,
# the pharyngeal wall, the soft palate, the tongue, and the upper lip.
trained_classes = sorted([
    ARYTENOID_CARTILAGE,
    EPIGLOTTIS,
    LOWER_LIP,
    PHARYNX,
    SOFT_PALATE_MIDLINE,
    THYROID_CARTILAGE,
    TONGUE,
    UPPER_LIP,
    VOCAL_FOLDS
])

classes_map = {0: "background"}
classes_map = {i + 1: c for i, c in enumerate(trained_classes)}
classes_dict = {c: i for i, c in classes_map.items()}


@lru_cache()
def load_model(device, state_dict_filepath=None):
    """
    Loads the Mask R-CNN model with the trained weights given by state_dict_filepath.

    Args:
    device (str): Device to load the neural network.
    state_dict_filepath (str): Path to the NN state dict.
    """
    state_dict_filepath = state_dict_filepath or BORDER_SEGMENTATION_FILEPATH

    device = torch.device(device)
    model = maskrcnn_resnet50_fpn(weights=MaskRCNN_ResNet50_FPN_Weights.DEFAULT)
    for p in model.parameters():
        p.requires_grad = False

    state_dict = torch.load(state_dict_filepath, map_location=device)
    model.load_state_dict(state_dict)
    model.to(device)
    model.eval()

    return model


def process_image_output(image_outputs):
    """
    From the output of the Mask R-CNN network, get the maximum score for each trained class and
    return the respective bounding box, score, and segmentation mask. If a given class was not
    detected, return None for that class instead.

    Args:
    image_outputs (Dict): Image outputs returned by Mask R-CNN.
    """
    zipped = list(zip(
        image_outputs["boxes"],
        image_outputs["labels"],
        image_outputs["scores"],
        image_outputs["masks"]
    ))

    im_detected = {}
    for c_idx, c_name in classes_map.items():
        art_list = funcy.lfilter(lambda t: t[1].item() == c_idx, zipped)

        if len(art_list) > 0:
            art = max(art_list, key=lambda t: t[2])
            im_detected[c_name] = {
                "bbox": art[0].detach().cpu().numpy(),
                "score": art[2].item(),
                "mask": art[3].detach().cpu().squeeze(dim=0).numpy()
            }
        else:
            im_detected[c_name] = None

    return im_detected


def detect_borders(batch, device="cpu", model=None):
    """
    Segment the borders of the articulators in a batch of MR images.

    Args:
    batch (torch.tensor): Batch of MR images with shape (B, C, W, H).
    device (str): Device to run the inference. Should be a device supported by PyTorch.
    model (torch.nn.Module): Loaded model. If None is given, loads the default model.
    """
    if "cuda" in device and not torch.cuda.is_available():
        raise Exception("CUDA is not available.")

    if len(batch.shape) != 4:
        raise Exception(f"Invalid batch size '({batch.shape})'. Expected (B, C, W, H).")

    if model is None:
        model = load_model(device)

    batch = batch.to(device)
    outputs = model(batch)
    detected = funcy.lmap(process_image_output, outputs)

    return detected
